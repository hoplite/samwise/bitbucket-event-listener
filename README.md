# bitbucket-event-listener

A microservice that listens for bitbucket events like branch creations, pull requests, trigger events. It then sends a message to the message api, and triggers a gitlab pipeline if necessary.